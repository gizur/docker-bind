DNS server 
===========

Build with: `docker build --rm -t dns .`

Run with: `docker run -d -p 53:53 dns`

IMPORTANT: `serial` needs to be updated each time `db.t.gizur.com` is updated (in the same container)


TShould give 10.0.0.1: `dig @localhost -p 5353 dbserver.redis-dns.local A`

Should give empty answer: `dig @localhost -p 5353 dbserverrrr.redis-dns.local A`

Should give empty answer: `dig @localhost -p 5353 dbserver.redis-dns.local MX`
