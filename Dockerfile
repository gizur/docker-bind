# DNS server
#
# VERSION               0.0.1
#
# Links:
#  - https://help.ubuntu.com/community/BIND9ServerHowto
#
# Guidelines
# ----------
#
# * Always use ubuntu:latest. Problems with new ubuntu releases should be fixed before
#  moving new images into production.
#
# * Daemons are managed with supervisord.
#
# * Logging from all processes should be performed to `/var/log/supervisor/supervisord.log`.
#   The start script will `tail -f` this log so it shows up in `docker logs`. The log file of 
#   daemons that can't log to `/var/log/supervisor/supervisord.log` should also be tailed
#   in `start.sh`
# 


FROM     ubuntu:latest
MAINTAINER Jonas Colmsjö "jonas@gizur.com"

RUN apt-get update
RUN apt-get install -y dnsutils nano


# Install dns server
# ------------------

RUN apt-get install -y bind9


RUN touch /var/log/query.log
RUN chown bind /var/log/query.log

EXPOSE 53
CMD ["/usr/sbin/named","-g -u bind"]
