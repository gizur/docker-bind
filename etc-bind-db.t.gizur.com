;
; BIND data file for local loopback interface
;
$TTL	604800
@	IN	SOA	t.gizur.com. root.t.gizur.com. (
			      2		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@	IN	NS	t.gizur.com.
@	IN	A	127.0.0.1
@	IN	AAAA	::1

; other entries
test	IN	A	10.0.0.1